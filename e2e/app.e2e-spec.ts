import { HelloWeatherPage } from './app.po';

describe('hello-weather App', () => {
  let page: HelloWeatherPage;

  beforeEach(() => {
    page = new HelloWeatherPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
