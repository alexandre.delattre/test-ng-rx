import { TestBed, async } from '@angular/core/testing';

import { AppComponent } from './app.component';
import {CityInputComponent} from "./components/city-input/city-input.component";
import {WeatherInfoComponent} from "./components/weather-info/weather-info.component";
import {WeatherService} from "./services/weather.service";
import {WeatherApiService} from "./services/weather-api.service";
import {HttpModule} from "@angular/http";

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent, CityInputComponent, WeatherInfoComponent
      ],
      providers: [WeatherService, {provide: WeatherApiService, useValue: {
        getWeather() {}
      }}],
      imports: [HttpModule]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
