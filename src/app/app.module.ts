import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { CityInputComponent } from './components/city-input/city-input.component';
import {WeatherInfoComponent} from './components/weather-info/weather-info.component';
import {WeatherApiService} from "./services/weather-api.service";
import {WeatherService} from "./services/weather.service";

@NgModule({
  declarations: [
    AppComponent,
    CityInputComponent,
    WeatherInfoComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [WeatherApiService, WeatherService,
    {provide: "OWMAppId", useValue: "f5ec19e734e184166a32f31603ffe45b"}
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
