import {TestBed, inject, fakeAsync, tick} from '@angular/core/testing';
import { WeatherService } from './weather.service';
import {WeatherApiService} from "./weather-api.service";
import {Observable, TestScheduler} from "rxjs";
import {Http, HttpModule} from "@angular/http";

let cities = { "t" : "toulouse", "b" : "bordeaux", "n" : null};
let loading = { "t" : true, "f" : false};
let weathers = {
    "t": {
        city: "toulouse"
    },
    "b": {
        city: "bordeaux"
    },
    "e": new Error(""),
    "n": null
};

describe('WeatherService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
        imports: [HttpModule],
      providers: [WeatherService, {provide: WeatherApiService, useValue: {
            getWeather() {}
      }}]
    });
  });

  it('should ...', inject([WeatherService, WeatherApiService], fakeAsync((service: WeatherService, apiService: WeatherApiService) => {
      let scheduler = new TestScheduler((a, b) => expect(a).toEqual(b));
      let inputs = scheduler.createColdObservable(         "--t--b----t", cities);
      spyOn(apiService, "getWeather").and.returnValues(
          scheduler.createColdObservable(                    "-t|", weathers),
          scheduler.createColdObservable(                       "--b|", weathers),
          scheduler.createColdObservable(                            "e|", weathers)
      );

      inputs.subscribe(service.currentCity);
      scheduler.expectObservable(service.currentCity).toBe("n-t--b----t", cities);
      scheduler.expectObservable(service.loading).toBe(    "f-tf-t-f--(tf)", loading);
      scheduler.expectObservable(service.weather).toBe(    "---t---b--n", weathers);
      scheduler.expectObservable(service.error).toBe(      "---n---n--e", weathers);
      scheduler.flush()
    }))
  )

   it('should display latest weather', inject([WeatherService, WeatherApiService], fakeAsync((service: WeatherService, apiService: WeatherApiService) => {
        let scheduler = new TestScheduler((a, b) => expect(a).toEqual(b));
        let inputs = scheduler.createColdObservable(         "--t--b", cities);

        spyOn(apiService, "getWeather").and.returnValues(
            scheduler.createColdObservable(                    "----t|", weathers),
            scheduler.createColdObservable(                        "-b|", weathers),
        );

        inputs.subscribe(service.currentCity);
        scheduler.expectObservable(service.currentCity).toBe("n-t--b", cities);
        scheduler.expectObservable(service.loading).toBe(    "f-t--tf", loading);
        scheduler.expectObservable(service.weather).toBe(    "------b", weathers);
        scheduler.expectObservable(service.error).toBe(      "------n", weathers);
        scheduler.flush()
    }))
   )

});
