import {Injectable, Inject} from '@angular/core';
import {Observable} from "rxjs";
import {WeatherInfo} from "../models/WeatherInfo";
import {Http, URLSearchParams} from "@angular/http";

@Injectable()
export class WeatherApiService {

    constructor(private http: Http, @Inject("OWMAppId") private apiKey: string) {
    }

    getWeather(city: string): Observable<WeatherInfo | Error> {
        let params = new URLSearchParams();
        params.set("q", city);
        params.set("appid", this.apiKey);
        params.set("units", "metric");

        return this.http.get("http://api.openweathermap.org/data/2.5/weather", {search: params}).map(
            r => {
                let data = r.json();
                return {
                    city: data.name,
                    temp: data.main.temp,
                    tempMin: data.main.temp_min,
                    tempMax: data.main.temp_max
                }
            }
        ).catch(e => {
            console.log("error", e);
            return Observable.of(new Error())
        });
    }
}
