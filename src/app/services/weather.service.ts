import { Injectable } from '@angular/core';
import {WeatherApiService} from "./weather-api.service";
import {BehaviorSubject, Observable} from "rxjs";
import {isWeatherInfo, WeatherInfo} from "../models/WeatherInfo";

@Injectable()
export class WeatherService {
  currentCity = new BehaviorSubject<string>(null);
  loading: Observable<boolean>;
  weather: Observable<WeatherInfo>;
  error: Observable<Error>;

  constructor(private apiService: WeatherApiService) {
    let loading = new BehaviorSubject<boolean>(false);
    const stream = this.currentCity
        .filter(c => c != null)
        .do(c => loading.next(true))
        .switchMap(city => apiService.getWeather(city))
        .do(c => loading.next(false))
        .share();

    this.weather = stream.map(r => isWeatherInfo(r) ? r : null);
    this.error = stream.map(r => !isWeatherInfo(r) ? r : null);
    this.loading = loading;
  }



}
