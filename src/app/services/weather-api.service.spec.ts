import {TestBed, inject, fakeAsync, tick} from '@angular/core/testing';

import {WeatherApiService} from './weather-api.service';
import {
    HttpModule, ConnectionBackend, RequestOptions, BaseRequestOptions, Http, Response,
    ResponseOptions
} from "@angular/http";
import {MockBackend, MockConnection} from "@angular/http/testing";

describe('WeatherApiService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                WeatherApiService,
                {provide: ConnectionBackend, useClass: MockBackend},
                {provide: RequestOptions, useClass: BaseRequestOptions},
                Http,
                {provide: "OWMAppId", useValue: "fakeId"}
            ]
        });
    });


    it('should call OpenWeatherMap API to provide weather information',
        fakeAsync(inject([WeatherApiService, ConnectionBackend], (service: WeatherApiService, mockBackend: MockBackend) => {
            let conn: MockConnection;
            let weather;

            mockBackend.connections.subscribe(c => {
                conn = c;
            });
            service.getWeather("london").subscribe(res => {
                weather = res;
            });
            conn.mockRespond(new Response(new ResponseOptions({
                status: 200,
                body: HTTP_SAMPLE
            })));

            tick();

            expect(conn.request.url).toBe('http://api.openweathermap.org/data/2.5/weather?q=london&appid=fakeId&units=metric');
            expect(service).toBeTruthy();
            expect(weather).toBeTruthy();
            expect(weather.city).toBe("London");

        })));
});


let HTTP_SAMPLE = `{"coord": {
"lon": -0.13,
"lat": 51.51
},
"weather": [
{
"id": 300,
"main": "Drizzle",
"description": "light intensity drizzle",
"icon": "09d"
}
],
"base": "stations",
"main": {
"temp": 280.32,
"pressure": 1012,
"humidity": 81,
"temp_min": 279.15,
"temp_max": 281.15
},
"visibility": 10000,
"wind": {
"speed": 4.1,
"deg": 80
},
"clouds": {
"all": 90
},
"dt": 1485789600,
"sys": {
"type": 1,
"id": 5091,
"message": 0.0103,
"country": "GB",
"sunrise": 1485762037,
"sunset": 1485794875
},
"id": 2643743,
"name": "London",
"cod": 200}`