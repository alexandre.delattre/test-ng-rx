import {Component, OnInit, ChangeDetectionStrategy} from "@angular/core";
import "rxjs/add/operator/map";
import {WeatherService} from "./services/weather.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit {
  service: WeatherService;

  constructor(service: WeatherService) {
    this.service = service;
  }

  ngOnInit(): void { }
}
