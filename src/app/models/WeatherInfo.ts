/**
 * Created by alexandre on 13/03/2017.
 */

export interface WeatherInfo {
    city: string,
    temp: number,
    tempMin: number,
    tempMax: number
}

export function isWeatherInfo(w): w is WeatherInfo {
    return (<any> w).city != null;
}