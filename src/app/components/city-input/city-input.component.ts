import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {Observable} from "rxjs";

@Component({
  selector: 'app-city-input',
  templateUrl: './city-input.component.html',
  styleUrls: ['./city-input.component.css']
})
export class CityInputComponent implements OnInit {
  @Output() citySelected = new EventEmitter<string>();

  constructor() { }

  ngOnInit() { }
}
