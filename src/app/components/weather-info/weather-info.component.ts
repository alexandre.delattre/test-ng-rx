import {Component, OnInit, Input, ChangeDetectionStrategy} from '@angular/core';
import {Subject, Observable, BehaviorSubject} from "rxjs";
import {WeatherInfo, isWeatherInfo} from "../../models/WeatherInfo";

@Component({
  selector: 'app-weather-info',
  templateUrl: './weather-info.component.html',
  styleUrls: ['./weather-info.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WeatherInfoComponent implements OnInit {
  @Input() weather: WeatherInfo;

  constructor() { }

  ngOnInit() { }
}
